![image](https://www.swishschool.com/w/wp-content/uploads/2018/04/%D8%A8%D8%B3%D9%85-%D8%A7%D9%84%D9%84%D9%87-%D8%A7%D9%84%D8%B1%D8%AD%D9%85%D9%86-%D8%A7%D9%84%D8%B1%D8%AD%D9%8A%D9%85-3.jpg)
# Introduction to R language

• This file contains a basic introduction and main principles of R language for the purpose of statistical computational processes.

>  Contains the following:

       ▀ How to manipulate with vectors and matrices and applying vectorisation processes.
         → Vectorisation applications (Addition - Substraction - Multiplication - Division)
         → Sequencing and logic convergence.  
       ▀ How to create lists and data frames.

![image](https://i.ibb.co/Gnc8vgD/Elex-AI.jpg)
       



## Roadmap
This file is allocated for introductory level for who desires to start data science track with a perfect language that highly recommended for statistical computation processes and data analysis.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.






 
